
/* mein Code */


PROC SQL;
   CREATE TABLE WORK.QUERY_FOR_CARS_Code AS 
   SELECT t1.Make, 
          t1.Model, 
          t1.Type, 
          /* AVG_of_MSRP */
            (AVG(t1.MSRP)) FORMAT=DOLLAR8. AS AVG_of_MSRP, 
          /* AVG_of_Invoice */
            (AVG(t1.Invoice)) FORMAT=DOLLAR8. AS AVG_of_Invoice, 
          /* AVG_of_Horsepower */
            (AVG(t1.Horsepower)) AS AVG_of_Horsepower, 
          /* AVG_of_Weight */
            (AVG(t1.Weight)) AS AVG_of_Weight
      FROM SASHELP.CARS t1
      WHERE t1.Make IN 
           (
           'Audi',
           'Acura'
           )
      GROUP BY t1.Make,
               t1.Model,
               t1.Type;
QUIT;

/* tolle Tabelle --> output erzeugen */
proc print data=QUERY_FOR_CARS_Code noobs;
run;